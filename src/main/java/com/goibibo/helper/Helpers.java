package com.goibibo.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.goibibo.Constants;

public class Helpers {
	
	private static Logger logger=LoggerFactory.getLogger(Helpers.class);
	public static Map<String, Integer> stub=new HashMap<>();
    public static Path namedColumnCsvPath() throws URISyntaxException {
        URI uri = ClassLoader.getSystemResource(Constants.GOIBIBO_CSV).toURI();
        return Paths.get(uri);
    }

    public static String readFile(Path path) {
    	logger.info("--> Inside readFile( ) ,Path ->"+path);
        String response = "";
        try {
            FileReader fr = new FileReader(path.toString());
            BufferedReader br = new BufferedReader(fr);
            String strLine;
            StringBuffer sb = new StringBuffer();
            int count=0;
            while ((strLine = br.readLine()) != null) {
            	System.out.println("strLine "+strLine);
                sb.append(strLine);
                if(count>0 && isInteger(strLine.split(",")[1])) {
                	stub.put(strLine.split(",")[0], Integer.parseInt(strLine.split(",")[1]));
                }else {
                	if(count>0) {
                		logger.warn("Ignoring String value while Parsing");
                		logger.error("Error while Parsing Value '"+ strLine.split(",")[1] +"' is not Integer ");
                	}
                }
                count++;
            }
            response = sb.toString();
            fr.close();
            br.close();
        } catch (Exception ex) {
            Helpers.err(ex);
        }
        return response;
    }
  
    public static boolean isInteger(String s) {
        try { 
            Integer.parseInt(s); 
        } catch(NumberFormatException e) { 
            return false; 
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }
    public static void err(Exception ex) {
        System.out.println(Constants.GENERIC_EXCEPTION + " " + ex);
    }
}
