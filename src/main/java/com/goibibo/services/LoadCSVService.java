package com.goibibo.services;

import java.nio.file.Path;

import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;
import com.goibibo.helper.Helpers;

@Service
public class LoadCSVService {
	
	@PostConstruct
	public void init() {
		Path path;
		try {
			 path=Helpers.namedColumnCsvPath();
		}catch(Exception e) {
			e.printStackTrace();
			return;
		}
		System.out.println(Helpers.readFile(path));
		System.out.println(Helpers.stub);
	}
	
	
	
	
}
