package com.goibibo.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.goibibo.helper.Helpers;
import com.goibibo.pojo.CSVReaderPojo;

@RestController
public class CSVController {

	@GetMapping("/{key}")
	public ResponseEntity<CSVReaderPojo> getCSVData(@PathVariable(name="key") String key){
		CSVReaderPojo result=new CSVReaderPojo();
			if(Helpers.stub.containsKey(key)) {
				System.out.println("Key: "+key+","+"value:"+Helpers.stub.get(key));
				result.setKey(key);
				result.setValue(Helpers.stub.get(key));
			}
		return new ResponseEntity<CSVReaderPojo>(result,HttpStatus.OK);
	}
}
